package com.korbiak.factory;

import com.korbiak.Model;
import com.korbiak.pizza.Pizza;
import com.korbiak.pizza.lviv.Cheese;
import com.korbiak.pizza.lviv.Clam;
import com.korbiak.pizza.lviv.Veggie;

public class LvivFactory extends Factory {
    @Override
    protected Pizza createPizza(Model model) {
        Pizza pizza = null;

        if (model == Model.CHEESE) {
            pizza = new Cheese();
        } else if (model == Model.CLAM) {
            pizza = new Clam();
        } else if (model == Model.VEGGIE) {
            pizza = new Veggie();
        }

        return pizza;
    }
}
