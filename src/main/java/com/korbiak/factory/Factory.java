package com.korbiak.factory;

import com.korbiak.Model;
import com.korbiak.pizza.Pizza;

public abstract class Factory {

    protected abstract Pizza createPizza(Model model);

    public Pizza assemble(Model model) {
        Pizza pizza = createPizza(model);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
