package com.korbiak.factory;

import com.korbiak.Model;
import com.korbiak.pizza.Pizza;
import com.korbiak.pizza.kyiv.Cheese;
import com.korbiak.pizza.kyiv.Clam;
import com.korbiak.pizza.kyiv.Veggie;

public class KyivFactory extends Factory {

    @Override
    protected Pizza createPizza(Model model) {
        Pizza pizza = null;

        if (model == Model.CHEESE) {
            pizza = new Cheese();
        } else if (model == Model.CLAM) {
            pizza = new Clam();
        } else if (model == Model.VEGGIE) {
            pizza = new Veggie();
        }

        return pizza;
    }
}
