package com.korbiak.pizza;

public interface Pizza {

    void prepare();

    void bake();

    void cut();

    void box();
}
