package com.korbiak.pizza.dnipro;

import com.korbiak.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Veggie implements Pizza {
    private Logger logger = LogManager.getLogger(Cheese.class);

    public String dough;
    public String sauce;
    public String[] toppings;


    @Override
    public void prepare() {
        logger.info("Prepare Veggie (Dnipro)");
    }

    @Override
    public void bake() {
        logger.info("Bake Veggie (Dnipro)");
        dough = "thick";
    }

    @Override
    public void cut() {
        logger.info("Cut Veggie (Dnipro)");
        sauce = "Marinara";
        toppings = new String[2];
        toppings[0] = "cheese";
        toppings[1] = "paper";
    }

    @Override
    public void box() {
        logger.info("Box Veggie (Dnipro)");
    }
}
