package com.korbiak.pizza.dnipro;

import com.korbiak.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Cheese implements Pizza {
    private Logger logger = LogManager.getLogger(Cheese.class);

    public String dough;
    public String sauce;
    public String[] toppings;


    @Override
    public void prepare() {
        logger.info("Prepare Cheese (Dnipro)");
    }

    @Override
    public void bake() {
        logger.info("Bake Cheese (Dnipro)");
        dough = "thick";
    }

    @Override
    public void cut() {
        logger.info("Cut Cheese (Dnipro)");
        sauce = "Marinara";
        toppings = new String[2];
        toppings[0] = "cheese";
        toppings[1] = "tomato";
    }

    @Override
    public void box() {
        logger.info("Box Cheese (Dnipro)");
    }
}
