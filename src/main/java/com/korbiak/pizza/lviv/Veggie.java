package com.korbiak.pizza.lviv;

import com.korbiak.pizza.Pizza;
import com.korbiak.pizza.dnipro.Cheese;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Veggie implements Pizza {
    private Logger logger = LogManager.getLogger(Cheese.class);

    public String dough;
    public String sauce;
    public String[] toppings;


    @Override
    public void prepare() {
        logger.info("Prepare Veggie (Lviv)");
    }

    @Override
    public void bake() {
        logger.info("Bake Veggie (Lviv)");
        dough = "thick";
    }

    @Override
    public void cut() {
        logger.info("Cut Veggie (Lviv)");
        sauce = "Marinara";
        toppings = new String[2];
        toppings[0] = "cheese";
        toppings[1] = "paper";
    }

    @Override
    public void box() {
        logger.info("Box Veggie (Lviv)");
    }
}
