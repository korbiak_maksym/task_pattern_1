package com.korbiak.pizza.kyiv;

import com.korbiak.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Cheese implements Pizza {
    private Logger logger = LogManager.getLogger(Cheese.class);

    public String dough;
    public String sauce;
    public String[] toppings;


    @Override
    public void prepare() {
        logger.info("Prepare Cheese (Kyiv)");
    }

    @Override
    public void bake() {
        logger.info("Bake Cheese (Kyiv)");
        dough = "thick";
    }

    @Override
    public void cut() {
        logger.info("Cut Cheese (Kyiv)");
        sauce = "Marinara";
        toppings = new String[3];
        toppings[0] = "cheese";
        toppings[1] = "tomato";
        toppings[2] = "tomato";
    }

    @Override
    public void box() {
        logger.info("Box Cheese (Kyiv)");
    }
}
