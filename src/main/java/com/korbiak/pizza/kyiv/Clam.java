package com.korbiak.pizza.kyiv;

import com.korbiak.pizza.Pizza;
import com.korbiak.pizza.dnipro.Cheese;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Clam implements Pizza {
    private Logger logger = LogManager.getLogger(Cheese.class);

    public String dough;
    public String sauce;
    public String[] toppings;


    @Override
    public void prepare() {
        logger.info("Prepare Clam (Kyiv)");
    }

    @Override
    public void bake() {
        logger.info("Bake Clam (Kyiv)");
        dough = "crust";
    }

    @Override
    public void cut() {
        logger.info("Cut Clam (Kyiv)");
        sauce = "Plum Tomato";
        toppings = new String[2];
        toppings[0] = "clam";
        toppings[1] = "paper";
    }

    @Override
    public void box() {
        logger.info("Box Clam (Kyiv)");
    }
}
