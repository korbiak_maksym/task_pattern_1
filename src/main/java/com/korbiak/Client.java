package com.korbiak;

import com.korbiak.factory.Factory;
import com.korbiak.factory.LvivFactory;
import com.korbiak.pizza.Pizza;

public class Client {

    public static void main(String[] args) {
        Factory factory = new LvivFactory();
        Pizza pizza = factory.assemble(Model.CLAM);
    }

}
